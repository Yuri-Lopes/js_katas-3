

const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];

//Kata 1 - Números de 1 a 25
function kata1() {
    let meu_conteudo = document.getElementById("kata1");
    let resultado = document.createElement("p");
    for (let i = 1; i <= 25; i++) {
        resultado.textContent += i + ", ";
        meu_conteudo.appendChild(resultado);
    }

}

kata1();


// Kata 2 - Números de 25 a 1
function kata2() {
    let meu_conteudo = document.getElementById("kata2");
    let resultado = document.createElement("p");
    for (let i = 25; i >= 1; i--) {
        resultado.textContent += i + ", ";
        meu_conteudo.appendChild(resultado);
    }

}

kata2();

// Kata 3 - Números de -1 a -25
function kata3() {
    let meu_conteudo = document.getElementById("kata3");
    let resultado = document.createElement("p");
    for (let i = -1; i >= -25; i--) {
        resultado.textContent += i + ", ";
        meu_conteudo.appendChild(resultado);
    }

}

kata3();

//Kata 4 - Números de -25 a -1
function kata4() {
    let meu_conteudo = document.getElementById("kata4");
    let resultado = document.createElement("p");
    for (let i = -25; i <= -1; i++) {
        resultado.textContent += i + ", ";
        meu_conteudo.appendChild(resultado);
    }

}

kata4();

// Kata 5 - Números ímpares de 25 a -25
function kata5() {
    let meu_conteudo = document.getElementById("kata5");
    let resultado = document.createElement("p");
    for (let i = 25; i >= -25; i -= 2) {
        resultado.textContent += i + ", ";
        meu_conteudo.appendChild(resultado);
    }

}

kata5();


//Kata 6 - Números divisíveis por 3 (até o 100)

function kata6() {
    let meu_conteudo = document.getElementById("kata6");
    let resultado = document.createElement("p");
    for (let i = 3; i <= 100; i += 3) {
        resultado.textContent += i + ", ";
        meu_conteudo.appendChild(resultado);
    }

}

kata6();

//kata 7 - Números divisíveis por 7 (até o 100
function kata7() {
    let meu_conteudo = document.getElementById("kata7");
    let resultado = document.createElement("p");
    for (let i = 7; i <= 100; i += 7) {
        resultado.textContent += i + ", ";
        meu_conteudo.appendChild(resultado);
    }

}

kata7();

//kata 8 - Números divisíveis por 3 e 7 (a partir de 100)
function kata8() {
    let meu_conteudo = document.getElementById("kata8");
    let resultado = document.createElement("p");
    for (let i = 100; i >= 3; i--) {
        if (i % 3 == 0 || i % 7 == 0) {
            resultado.textContent += i + ", ";
        }

        meu_conteudo.appendChild(resultado);
    }

}

kata8();

//kata 9 - Números ímpares divisíveis por 5 (até o 100)
function kata9() {
    let meu_conteudo = document.getElementById("kata9");
    let resultado = document.createElement("p");
    for (let i = 5; i <= 95; i++) {
        if (i % 2 === 1 && i % 5 === 0) {
            resultado.textContent += i + ", ";
        }

        meu_conteudo.appendChild(resultado);
    }

}

kata9();

//kata 10 - Exibir 20 elementos de sampleArray
function kata10() {
    let meu_conteudo = document.getElementById("kata10");
    let resultado = document.createElement("p");
    for (let i = 0; i < sampleArray.length; i++) {
        resultado.textContent += sampleArray[i] + ", ";
        meu_conteudo.appendChild(resultado);
    }

}

kata10();

//kata 11 - Exibir pares de sampleArray
function kata11() {
    let meu_conteudo = document.getElementById("kata11");
    let resultado = document.createElement("p");
    for (let i = 0; i < sampleArray.length; i++) {
        if (sampleArray[i] % 2 === 0) {
            resultado.textContent += sampleArray[i] + ", ";
            meu_conteudo.appendChild(resultado);
        }

    }

}

kata11();

//kata 12 - Exibir ímpares de sampleArray
function kata12() {
    let meu_conteudo = document.getElementById("kata12");
    let resultado = document.createElement("p");
    for (let i = 0; i < sampleArray.length; i++) {
        if (sampleArray[i] % 2 === 1) {
            resultado.textContent += sampleArray[i] + ", ";
            meu_conteudo.appendChild(resultado);
        }

    }

}

kata12();

//kata 13 - Exibir divisíveis por 8 de sampleArray
function kata13() {
    let meu_conteudo = document.getElementById("kata13");
    let resultado = document.createElement("p");
    for (let i = 0; i < sampleArray.length; i++) {
        if (sampleArray[i] % 8 === 0) {
            resultado.textContent += sampleArray[i] + ", ";
            meu_conteudo.appendChild(resultado);
        }

    }

}

kata13();

//kata 14 - Exibir quadrado dos elementos de sampleArray
function kata14() {
    let meu_conteudo = document.getElementById("kata14");
    let resultado = document.createElement("p");
    for (let i = 0; i < sampleArray.length; i++) {
        resultado.textContent += sampleArray[i] * sampleArray[i] + ", ";
        meu_conteudo.appendChild(resultado);


    }

}

kata14();

//kata 15 - Soma de todos os números (1 a 20)
function kata15() {
    let meu_conteudo = document.getElementById("kata15");
    let resultado = document.createElement("p");
    let soma = 0;
    for (let i = 1; i <= 20; i++) {
        soma += i
    }
    resultado.textContent += soma + ", ";
    meu_conteudo.appendChild(resultado);

}

kata15();

//kata 16 - Soma dos elementos de sampleArray
function kata16() {
    let meu_conteudo = document.getElementById("kata16");
    let resultado = document.createElement("p");
    let soma = 0;
    for (let i = 0; i < sampleArray.length; i++) {
        soma += sampleArray[i]

    }
    resultado.textContent += soma + " ";
    meu_conteudo.appendChild(resultado);

}

kata16();

//kata 17 - Exibir o menor elemento de sampleArray
function kata17() {
    let meu_conteudo = document.getElementById("kata17");
    let resultado = document.createElement("p");
    let min = sampleArray[0];
    for (let i = 1; i < sampleArray.length; i++) {
        if (sampleArray[i] < min) {
            min = sampleArray[i]

        }

    }
    resultado.textContent = min;
    meu_conteudo.appendChild(resultado);

}

kata17();

//kata 18 - Exibir o maior elemento de sampleArray
function kata18() {
    let meu_conteudo = document.getElementById("kata18");
    let resultado = document.createElement("p");
    let max = sampleArray[0];
    for (let i = 1; i < sampleArray.length; i++) {
        if (sampleArray[i] > max) {
            max = sampleArray[i]

        }

    }
    resultado.textContent = max;
    meu_conteudo.appendChild(resultado);

}

kata18();

